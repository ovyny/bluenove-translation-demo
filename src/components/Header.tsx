import React from 'react';
import { useTranslation } from "react-i18next";
import styled from 'styled-components';

const Header = () => {
  const [t, i18n] = useTranslation('common');

  const Menu = styled.div`
        width: 100%;
        margin: 0;
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        background-color: #00A5E1;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,.4);
    `;

    const Title = styled.h1`
        color: #FFFFFF;
        display: flex;
        margin-left: 25px;
    `;

    const ChangeLanguageWrapper = styled.div`
        display: flex;
        align-items: center;
        justifiy-content: center;
        padding: 0px 25px;
    `;

    const ChangeLanguageButton = styled.button`
        border-radius: 8px;
        padding: 10px 20px;
        border: none;
        background-color: #FFFFFF;
        margin-left: 5px;
        color: #00A5E1;
    `;

  return (
    <Menu>
        <Title>{t('welcome.title', { application: 'dashboard' })}</Title>
        <ChangeLanguageWrapper>
            <ChangeLanguageButton onClick={() => i18n.changeLanguage('fr')}>FR</ChangeLanguageButton>
            <ChangeLanguageButton onClick={() => i18n.changeLanguage('en')}>EN</ChangeLanguageButton>
        </ChangeLanguageWrapper>
    </Menu>
  );
}

export default Header;