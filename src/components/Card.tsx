import React from 'react';
import { useTranslation } from "react-i18next";
import styled from 'styled-components';

interface CardProps {
    tag: string;
    message: string;
    messageParams?: any;
};

const Card = ({
    tag,
    message,
    messageParams = {}
}: CardProps) => {
    const [t, i18n] = useTranslation('common');

    const Card = styled.div`
        padding: 15px;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,.4);
        border-radius: 17px;
        width: 100%;
        margin: 10px 5px;

        @media (min-width: 768px) {
            margin: 15px 15px;
        }        
    `;

    const Tag = styled.div`
        border-radius: 17px;
        display: inline-block;
        background-color: ${(p: { type: string }) => p.type === 'alert' ? '#FF4747' : p.type === 'info' ? '#1E8449' : '	#FF8C00'};
        color: #FFFFFF;
        padding: 10px 20px;
    `;

    return (
        <Card>
            <Tag type={tag}>{t(`tag.${tag}`)}</Tag>
            <p>{t(message, messageParams)}</p>
        </Card>
    );
}

export default Card;