import React, { Suspense } from 'react';
import styled from 'styled-components';
import Header from './components/Header';
import Card from './components/Card';

const App = () => {
  const Container = styled.div`
    width: 100%;
    padding-top: 85px;

    @media only screen and (max-width: 768px) {
      padding-top: 115px;
    }

    @media only screen and (max-width: 415px) {
      padding-top: 145px;
    }
  `;

  const Row = styled.div`
    width: 98%;
    display: flex;
    flex-direction: column;  

    @media (min-width: 768px) {
      padding-left: 15px;
      padding-right: 15px;
      flex-direction: row;
      justify-content: space-between;
    }
  `;

  return (
    <Suspense fallback="loading">
      <Header />
      <Container>
        <Row>
          <Card tag='alert' message='message.deleteAccountRequest' messageParams={{ userName: 'Pierre DUPONT' }} />
          <Card tag='warning' message='message.infoUpdateRequest' />
          <Card tag='alert' message='message.deletedAccount' messageParams={{ userName: 'Pierre DUPONT' }} />
        </Row>
        <Row>
          <Card tag='info' message='message.birthday' messageParams={{ userName: 'Jean-Michel DUPONT' }} />
          <Card tag='alert' message='message.deleteAccountRequest' messageParams={{ userName: 'Pierre DUPONT' }} />
          <Card tag='warning' message='message.infoUpdateRequest' />
        </Row>
        <Row>
          <Card tag='alert' message='message.deletedAccount' messageParams={{ userName: 'Pierre DUPONT' }} />
          <Card tag='info' message='message.birthday' messageParams={{ userName: 'Jean-Michel DUPONT' }} />
          <Card tag='alert' message='message.deletedAccount' messageParams={{ userName: 'Pierre DUPONT' }} />
        </Row>
      </Container>
    </Suspense>
  );
}

export default App;